package mt.box;

public class Box2 {
    public static final int N_LOOPS = 2;
    private boolean occupied;
    private final Object lock = new Object();

    private void producer() {
        for (int i = 0; i < N_LOOPS; i++) {
            log("started iteration #" + i);
            synchronized (lock) {
                log("has acquired the lock");
                wakeCounterParty();
                waitUntilFree();
                fillTheBox();
            }
            log("has released the lock, finished iteration #" + i);
        }
    }

    private void wakeCounterParty() {
        log("awaking other party");
        lock.notifyAll();
    }

    private void consumer() {
        for (int i = 0; i < N_LOOPS; i++) {
            log("started iteration #" + i);
            synchronized (lock) {
                log("has acquired the lock");
                wakeCounterParty();
                waitUntilNotEmpty();
                emptyTheBox();
            }
            log("has released the lock, finished iteration #" + i);
        }
    }

    private void log(String string) {
        System.out.println(System.nanoTime() + " " + Thread.currentThread().getName() + " " + string);

    }

    private void waitUntilNotEmpty() {
        while (!occupied) {
            log("box is empty");
            waitOnLock();
        }
    }

    private void waitOnLock() {
        try {
            lock.wait();
        } catch (InterruptedException e) {
            System.exit(1);
        }
        log("woke up");
    }

    private void emptyTheBox() {
        occupied = false;
        log("emptied the box");
    }

    private void fillTheBox() {
        occupied = true;
        log("filled the box");
    }

    private void waitUntilFree() {
        while (occupied) {
            log("box is occupied");
            waitOnLock();
        }
    }

    private class Producer implements Runnable {
        @Override
        public void run() {
            producer();
            log("done");
        }
    }

    private class Consumer implements Runnable {
        @Override
        public void run() {
            consumer();
            log("done");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10000; i++) {
            new Box2().run();
        }
    }

    private void run() throws InterruptedException {
        log("===== Starting new run =====");
        Thread consumer = new Thread(new Consumer(), "Consumer");
        Thread producer = new Thread(new Producer(), "Producer");
        consumer.start();
        producer.start();
        consumer.join();
        producer.join();
    }
}
