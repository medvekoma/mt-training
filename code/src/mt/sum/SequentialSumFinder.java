package mt.sum;

public class SequentialSumFinder implements SumFinder {
    @Override
    public long sum(int[] data) {
        return SummationLoop.sum(data, 0, data.length);
    }

    @Override
    public void shutdown() {
        // nothing to do here
    }
}
