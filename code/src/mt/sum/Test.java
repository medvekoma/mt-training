package mt.sum;

import java.util.Random;

public class Test {
	public static void main(String[] ignored) {
		System.out.println("System reports " + Runtime.getRuntime().availableProcessors() + " cores");
		Random r = new Random(2016);
		int[] data = new int[300_000_000];
		for (int i = 0; i < data.length; i++) {
			data[i] = r.nextInt();
		}

		test(new SequentialSumFinder(), data);

		// test(new ForkingRecursiveTaskSumFinder(50), data);
		// test(new ForkingRecursiveTaskSumFinder(500), data);
		test(new ForkingRecursiveTaskSumFinder(5_000), data);
		test(new ForkingRecursiveTaskSumFinder(50_000), data);
		test(new ForkingRecursiveTaskSumFinder(500_000), data);
		test(new ForkingRecursiveTaskSumFinder(5_000_00), data);
		test(new ForkingRecursiveTaskSumFinder(50_000_000), data);
		test(new ForkingRecursiveTaskSumFinder(100_000_000), data);
		test(new ForkingRecursiveTaskSumFinder(200_000_000), data);
		test(new ForkingRecursiveTaskSumFinder(300_000_000), data);
		test(new ForkingRecursiveTaskSumFinder(400_000_000), data);
		test(new ForkingRecursiveTaskSumFinder(500_000_000), data);

		test(new ForkingRecursiveActionSumFinder(5_000), data);
		test(new DirectRecursiveTaskSumFinder(5_000), data);
		test(new ForkingRecursiveTaskSumFinder(5_000), data);

		test(new ForkingRecursiveActionSumFinder(500_000), data);
		test(new DirectRecursiveTaskSumFinder(500_000), data);
		test(new ForkingRecursiveTaskSumFinder(50_000_000), data);

		test(new ForkingRecursiveActionSumFinder(100_000_000), data);
		test(new DirectRecursiveTaskSumFinder(100_000_000), data);
		test(new ForkingRecursiveTaskSumFinder(100_000_000), data);

		test(new ParallelStreamsSumFinder(), data);
	}

	private static void test(SumFinder sumFinder, int[] data) {
		long best = Long.MAX_VALUE;
		for (int i = 0; i < 10; i++) {
			long start = System.currentTimeMillis();
			long sum = sumFinder.sum(data);
			long end = System.currentTimeMillis();
			long duration = end - start;
			System.out.println(sumFinder + " found sum value " + sum + " in " + duration + "ms");
			best = Math.min(duration, best);
		}
		System.out.println(sumFinder + " best time: " + best + "ms");
		System.out.println("====");
		sumFinder.shutdown();
	}

}
