package mt.sum;

public class SummationLoop {
    public static long sum(int[] data, int fromIndex, int toIndex) {
        checkArgs(data, fromIndex, toIndex);
        checkArgs(data, fromIndex, toIndex);
        log("calculating [" + fromIndex + ", " + toIndex + "), size: " + (toIndex - fromIndex));
        long sum = 0;
        for (int i = fromIndex ; i < toIndex; i++) {
            sum += data[i];
        }
        log("done with [" + fromIndex + ", " + toIndex + "), size: " + (toIndex - fromIndex));
        return sum;
    }

    private static void checkArgs(int[] data, int fromIndex, int toIndex) {
        if (data.length == 0) {
            throw new IllegalArgumentException("data array must not be empty");
        }
        int maxIndex = data.length;
        checkIndex("fromIndex", fromIndex, 0, maxIndex);
        checkIndex("toIndex", toIndex, fromIndex, maxIndex);
    }

    private static void checkIndex(String name, int index, int minAccepted, int maxAccepted) {
        if (index < minAccepted || index > maxAccepted) {
            throw new IllegalArgumentException("Invalid " + name + ": " + index + ", should have been in [" + minAccepted + ".."  + maxAccepted + "]");
        }
    }

    private static void log(String message) {
        //System.out.println(System.currentTimeMillis() + " " + Thread.currentThread().getName() + " " + SummationLoop.class.getSimpleName() + " " + message);
    }
}