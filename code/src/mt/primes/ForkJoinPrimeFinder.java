package mt.primes;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import javax.management.RuntimeErrorException;

public class ForkJoinPrimeFinder implements PrimeFinder {
	private final long sequentialLimit;
	
	public ForkJoinPrimeFinder(long sequentialLimit) {
		this.sequentialLimit = sequentialLimit;
	}
	
	@Override
	public List<Long> findPrimes(long from, long to) {
		Task task = new Task(from, to);
		ForkJoinPool pool = new ForkJoinPool(4);
		System.out.println("Parallelism: " + pool.getParallelism());
		return pool.invoke(task);
	}
	
	private class Task extends RecursiveTask<List<Long>> {
		private final long from;
		private final long to;

		public Task(long from, long to) {
			this.from = from;
			this.to = to;
		}
	
		@Override
		protected List<Long> compute() {
			List<Long> primes;
			if (to - from < sequentialLimit) {
				primes = new SequentialPrimeFinder().findPrimes(from, to);
//				System.out.println(Thread.currentThread().getName() + " Sequential primes from " + from + " to " + to + " are: " + primes);
			} else {
				long mid = from + (to - from) / 2;
				Task left = new Task(from, mid);
				Task right = new Task(mid, to);
				left.fork();
//				System.out.println(Thread.currentThread().getName() + " forked " + left);
//				System.out.println(Thread.currentThread().getName() + " computing " + right);
				primes = right.compute();
//				System.out.println(Thread.currentThread().getName() + " back from " + right);
//				System.out.println(Thread.currentThread().getName() + " joining " + left);
				List<Long> leftList = left.join();
				primes.addAll(leftList);
			}
			return primes;
		}
		
		@Override
		public String toString() {
			return "Task[" + from + "-" + to + "]";
		}
	}
}